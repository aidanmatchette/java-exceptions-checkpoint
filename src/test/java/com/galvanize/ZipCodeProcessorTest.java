package com.galvanize;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ZipCodeProcessorTest {
    private ZipCodeProcessor processor;
    private Verifier verifier;
//
//
    @BeforeEach
    void setUp() {
        verifier = new Verifier();
        processor = new ZipCodeProcessor(verifier);
    }

    @Test
    void generateInvalidFormatException() {
        InvalidFormatException ie = new InvalidFormatException("test");
    }

    @Test
    void correctlyProcessValidZipCode() throws Exception {
        String zipCode = "80302";

        assertEquals("Thank you!  Your package will arrive soon.", processor.process(zipCode));
    }

    @Test
    void zipCodeIsTooShort() throws Exception {
        String zipCode = "8030";

        assertEquals("The zip code you entered was the wrong length.", processor.process(zipCode));
    }

    @Test
    void zipCodeIsTooLong() throws Exception {
        String zipCode = "804430";

        assertEquals("The zip code you entered was the wrong length.", processor.process(zipCode));
    }

    @Test
    void verifierTestImproperInput() {
        assertThrows(InvalidFormatException.class, () -> {
            verifier.verify("8030");
        });
    }

    @Test
    void verifierMessageTooShort() {
        InvalidFormatException exception = assertThrows(InvalidFormatException.class, () -> {
            verifier.verify("8030");
        });

        assertEquals("ERRCODE 22: INPUT_TOO_SHORT", exception.getMessage());
    }

    @Test
    void verifierMessageNoCoverage() {
        NoServiceException exception = assertThrows(NoServiceException.class, () -> {
            verifier.verify("12234");
        });

        assertEquals("ERRCODE 27: NO_SERVICE", exception.getMessage());
    }

    @Test
    void noServiceReturnString() throws Exception {

        String zipCode = "12234";

        assertEquals("We're sorry, but the zip code you entered is out of our range.", processor.process(zipCode));
    }
}