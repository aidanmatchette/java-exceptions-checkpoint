package com.galvanize;

/**
 * InvalidFormatException
 */
public class InvalidFormatException extends Exception {

    public InvalidFormatException(String message) {
        super(message);
    }

}
