package com.galvanize;

/**
 * NoServiceExeption
 */
public class NoServiceException extends Exception {

    public NoServiceException(String message) {
        super(message);
    }

}
